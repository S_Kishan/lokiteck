import {View, Text, ActivityIndicator, Image, SafeAreaView} from 'react-native';
import React from 'react';
import base from '../../Constants/CommonStyle';
import {useDispatch, useSelector} from 'react-redux';
import color from '../../Constants/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import * as Atoms from '../../Components/atoms';
import {userLogoutAction} from '../../Store/user';

const Profile = ({navigation}) => {
  const dispatch = useDispatch();

  const stateUser = useSelector(state => state.user);

  const [loaded, setLoaded] = React.useState(false);

  const [user, setUser] = React.useState(null);

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', e => {
      if (stateUser && stateUser.currentUser) {
        setUser(stateUser.currentUser);
      }
    });
    return unsubscribe;
  });

  return (
    <SafeAreaView style={[base.block, base.center, base.horizonalSpace]}>
      {user !== null && (
        <>
          <View
            style={{
              height: wp(43.5),
              width: wp(42.5),
              justifyContent: 'center',
              alignItems: 'center',
              backgroundColor: color._mediumGray,
              borderRadius: wp(40),
              marginRight: wp(3.5),
            }}>
            {!loaded && (
              <View
                style={{
                  height: wp(43.5),
                  width: wp(42.5),
                  justifyContent: 'center',
                  alignItems: 'center',
                  backgroundColor: color._mediumGray,
                  borderRadius: wp(40),
                  marginRight: wp(3.5),
                }}>
                <ActivityIndicator size="large" color="white" />
              </View>
            )}
            {user.image && (
              <Image
                source={{uri: user.image}}
                style={{
                  height: wp(43.5),
                  width: wp(42.5),
                  backgroundColor: color._mediumGray,
                  borderRadius: wp(40),
                  marginRight: wp(3.5),
                  position: 'absolute',
                }}
                onLoad={() => setLoaded(true)}
                resizeMethod="resize"
                resizeMode="contain"
              />
            )}
          </View>
          {user.firstName && user.lastName && (
            <Text
              style={[base.fontBold, {fontSize: wp(5), marginVertical: wp(3)}]}>
              {user.firstName + ' ' + user.lastName}
            </Text>
          )}
          {user.email && (
            <Text
              style={[base.fontBold, {fontSize: wp(5), marginBottom: wp(3)}]}>
              {user.email}
            </Text>
          )}

          <Atoms.Button
            title={'LOGOUT'}
            style={{
              backgroundColor: color._green,
              borderRadius: wp(3.25),
              marginVertical: wp(3.5),
            }}
            textStyle={{
              fontSize: wp(4.5),
              fontWeight: '700',
              color: color._blue,
            }}
            onPress={() => {
              let request = {};
              dispatch(userLogoutAction(request, navigation));
            }}
          />
        </>
      )}
    </SafeAreaView>
  );
};

export default Profile;
