import {StyleSheet} from 'react-native';
import color from '../../Constants/Color';
import fonts from '../../Constants/Fonts';

const styles = StyleSheet.create({
  btn: {fontWeight: 'bold'},
  btn1: {
    backgroundColor: color._transparent,
    elevation: 0,
    borderWidth: 1,
    borderColor: color._lightGray,
  },
  btnTxt: {
    fontFamily: fonts.SEMI_BOLD,
  },
  btnTxt1: {
    color: color._blue,
    fontFamily: fonts.SEMI_BOLD,
  },
});

export default styles;
