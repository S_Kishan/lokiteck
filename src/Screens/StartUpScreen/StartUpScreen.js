import React, {useEffect} from 'react';
import SplashScreen from 'react-native-splash-screen';
import {useSelector} from 'react-redux';

const StartUpScreen = ({navigation}) => {
  const stateUser = useSelector(state => state.user);

  useEffect(() => {
    setTimeout(() => {
      SplashScreen.hide();
      if (stateUser.isLogin) {
        navigation.navigate('Home');
      } else {
        navigation.navigate('Login');
      }
    }, 1500);
  });

  return <></>;
};

export default StartUpScreen;
