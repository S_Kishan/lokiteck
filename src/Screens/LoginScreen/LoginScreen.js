import {
  View,
  Text,
  SafeAreaView,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import React from 'react';
import styles from './LoginScreenStyle';
import base from '../../Constants/CommonStyle';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import * as Molecules from '../../Components/molecules';
import color from '../../Constants/Color';
import fonts from '../../Constants/Fonts';
import * as Atoms from '../../Components/atoms';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import {userLoginAction} from '../../Store/user';
import {useDispatch} from 'react-redux';

const LoginScreen = ({navigation}) => {
  const dispatch = useDispatch();

  const [mobile, setMobile] = React.useState('');
  const [mobileErr, setMobileErr] = React.useState(null);

  const [password, setPassword] = React.useState('');
  const [pass, setPass] = React.useState(false);
  const [passError, setPassError] = React.useState(null);

  const onLogin = () => {
    let valid = true;
    let error = null;
    if (mobile && mobile.length > 0) {
      valid = true;
    } else {
      valid = false;
      error = 'Please enter valid User Name.';
      setMobileErr('Please enter valid user name.');
    }
    if (password && password.length > 0) {
      if (password.length >= 6 && password.length <= 15) {
        valid = true;
      } else {
        valid = false;
        error = 'Passowrd must be more than 6 and less then 15 characters.';
        setPassError(
          'Passowrd must be more than 6 and less then 15 characters.',
        );
      }
    } else {
      valid = false;
      error = 'Please enter password.';
      setPassError('Please enter password.');
    }

    if (valid) {
      let request = {
        username: mobile,
        password: password,
      };
      dispatch(userLoginAction(request, navigation));
    } else {
    }
  };

  return (
    <SafeAreaView style={[base.block]}>
      <KeyboardAvoidingView
        style={[base.block]}
        keyboardShouldPersistTaps="always"
        showsVerticalScrollIndicator={false}>
        <ScrollView>
          <KeyboardAwareScrollView
            style={[base.block]}
            keyboardShouldPersistTaps="always"
            showsVerticalScrollIndicator={false}>
            <View style={[base.horizonalSpace, base.block, base.center]}>
              <Text
                style={{
                  fontSize: wp(4.5),
                  fontWeight: '700',
                  fontFamily: fonts.BOLD,
                  marginVertical: 6,
                  color: color._black,
                }}>
                Login
              </Text>

              <Molecules.InputField
                title={'User Name'}
                inputContainerStyle={{backgroundColor: color._white}}
                placeholder="Enter User Name"
                placeholderTextColor={color._mediumGray}
                value={mobile}
                error={mobileErr}
                onChangeText={text => {
                  setMobileErr(null);
                  setMobile(text);
                }}
              />

              <Molecules.InputField
                title={'Password'}
                // required
                error={passError}
                secureTextEntry={true}
                isRight={password.length !== 0 ? true : true}
                value={password}
                valueFocus={pass}
                inputContainerStyle={{backgroundColor: color._white}}
                placeholder="Enter Password"
                placeholderTextColor={color._mediumGray}
                onFocus={() => {
                  setPass(true);
                }}
                onChangeText={text => {
                  setPassError(null);
                  setPassword(text);
                }}
              />

              <Atoms.Button
                title={'SIGN IN'}
                style={{
                  backgroundColor: color._green,
                  borderRadius: wp(3.25),
                  marginVertical: 10,
                }}
                textStyle={{
                  fontSize: wp(4.75),
                  fontWeight: '700',
                  color: color._blue,
                }}
                onPress={() => {
                  onLogin();
                }}
              />
            </View>
          </KeyboardAwareScrollView>
        </ScrollView>
      </KeyboardAvoidingView>
    </SafeAreaView>
  );
};

export default LoginScreen;
