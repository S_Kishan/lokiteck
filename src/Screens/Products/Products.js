import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {
  getAllProducts,
  getProductsCategories,
  getSearchProducts,
  resetAddStatus,
} from '../../Store/products';
import base from '../../Constants/CommonStyle';
import * as Molecules from '../../Components/molecules';
import Search from '../../assets/icn_search.svg';
import color from '../../Constants/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styles from './ProductsStyle';

const Products = ({navigation}) => {
  const dispatch = useDispatch();

  const stateProduct = useSelector(state => state.products);

  const [products, setProducts] = React.useState([]);

  const [searchTxt, setSearchTxt] = React.useState('');

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', e => {
      let request = {};
      if (stateProduct) {
        if (
          stateProduct &&
          stateProduct.categories &&
          stateProduct.categories.length === 0
        ) {
          dispatch(getProductsCategories(request, navigation));
        }
        if (stateProduct.addSuccess && stateProduct.addedProduct) {
          setProducts([stateProduct.addedProduct, ...products]);
          dispatch(resetAddStatus());
        } else {
          let request = {};
          dispatch(getAllProducts(request, navigation));
        }
      }
    });
    return unsubscribe;
  });

  React.useEffect(() => {
    if (stateProduct) {
      if (stateProduct.productList) {
        setProducts(stateProduct.productList);
      }
    }
  }, [stateProduct.productList]);

  const renderProduct = ({item, index}) => {
    return <Molecules.ProductBlock details={item} />;
  };

  return (
    <SafeAreaView style={[base.block]}>
      <Molecules.AuthHeader
        back={false}
        _onBack={() => {}}
        title={'Products'}
        btnTitle={'Add Product'}
        _onRightBtnPress={() => {
          navigation.navigate('AddProduct');
        }}
      />
      <View style={[base.block, base.horizonalSpace]}>
        <Molecules.InputField
          inputContainerStyle={{
            marginTop: wp(2.75),
            height: wp(11),
          }}
          placeholder="Search Product"
          placeholderTextColor={color._mediumGray}
          value={searchTxt}
          onChangeText={text => {
            setSearchTxt(text);
          }}
          isRight
          rightContent={
            <TouchableOpacity
              style={[
                base.center,
                {
                  flex: 1,
                  marginLeft: -wp(13),
                  backgroundColor: color._lightGray,
                  borderRadius: wp(2.5),
                },
              ]}
              onPress={() => {
                let request = {
                  query: searchTxt,
                };
                dispatch(getSearchProducts(request, navigation));
              }}>
              <Search height={wp(9)} width={wp(9)} />
            </TouchableOpacity>
          }
        />

        {products.length > 0 ? (
          <FlatList
            data={products}
            keyExtractor={(item, index) => {
              return index.toString();
            }}
            renderItem={renderProduct}
          />
        ) : (
          <View style={[base.block, base.center]}>
            <Text style={[base.fontMedium]}>No Products Found.</Text>
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

export default Products;
