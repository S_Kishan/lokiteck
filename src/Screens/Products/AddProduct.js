import {
  View,
  SafeAreaView,
  FlatList,
  Image,
  TouchableOpacity,
  Keyboard,
} from 'react-native';
import React from 'react';
import base from '../../Constants/CommonStyle';
import styles from './ProductsStyle';
import * as Molecules from '../../Components/molecules';
import * as Atoms from '../../Components/atoms';
import * as ImagePicker from 'react-native-image-picker';
import * as Models from '../../Components/models';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import color from '../../Constants/Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import Remove from '../../assets/icn_remove_image.svg';
import {addProductAction} from '../../Store/products';
import {useDispatch, useSelector} from 'react-redux';
import {AlertHelper} from '../../Constants/AlertHelper';
import {AirbnbRating} from 'react-native-elements';

const AddProduct = ({navigation}) => {
  const dispatch = useDispatch();

  const stateProduct = useSelector(state => state.products);

  const [title, setTitle] = React.useState('');
  const [titleErr, setTitleErr] = React.useState(null);

  const [description, setDescription] = React.useState('');
  const [descriptionErr, setDescriptionErr] = React.useState(null);

  const [price, setPrice] = React.useState('');
  const [priceErr, setPriceErr] = React.useState(null);
  const [priceUnit, setPriceUnit] = React.useState('$');

  const [discount, setdiscount] = React.useState('');
  const [discountErr, setDiscountErr] = React.useState(null);
  const [discountUnit, setDiscountUnt] = React.useState('%');

  const [stock, setStock] = React.useState('');
  const [stockErr, setStockErr] = React.useState(null);

  const [brand, setBrand] = React.useState('');
  const [brandErr, setBrandErr] = React.useState(null);

  const [categoryList, setCategoryList] = React.useState([]);
  const [category, setCategory] = React.useState(null);
  const [categoryErr, setCategoryErr] = React.useState(null);

  const [rating, setRating] = React.useState(null);
  const [ratingErr, setRatingErr] = React.useState(null);

  const [multipleImages, setMultipleImages] = React.useState([]);
  const [modalVisible, setModalVisible] = React.useState(false);
  const [refreshing, setRefreshing] = React.useState(false);
  const [preview, setPreview] = React.useState(false);
  const [image, setImage] = React.useState(null);
  const [multipleImagesErr, setMultipleImagesErr] = React.useState(null);

  const [scrollEnabled, setScrollEnabled] = React.useState(true);

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', e => {
      if (stateProduct && stateProduct.categories) {
        setCategoryList(stateProduct.categories);
      }
    });
    return unsubscribe;
  });

  React.useEffect(() => {
    if (stateProduct && stateProduct.categories) {
      setCategoryList(stateProduct.categories);
    }
  }, [stateProduct.categories]);

  const chooseGalleryImage = () => {
    ImagePicker.launchImageLibrary({
      width: 300,
      height: 400,
      multiple: true,
      cropping: true,
      includeBase64: true,
    }).then(image => {
      if (!image.didCancel) {
        let images = multipleImages.concat(image.assets);
        setMultipleImages(images);
      }
      setRefreshing(!refreshing);
    });
  };
  const chooseCameraImage = () => {
    ImagePicker.launchCamera({
      width: 300,
      height: 400,
      cropping: true,
      includeBase64: true,
    }).then(image => {
      if (!image.didCancel) {
        let images = multipleImages.concat(image.assets);
        setMultipleImages(images);
      }
      setRefreshing(!refreshing);
    });
  };
  const removeImage = index => {
    let localres = multipleImages;
    localres.splice(index, 1);
    setMultipleImages(localres);
    setRefreshing(!refreshing);
  };
  const _renderFlatList = ({item, index}) => {
    return (
      <TouchableOpacity
        style={styles.uploadImgCol}
        onPress={() => {
          setPreview(!preview);
          setImage(item.uri);
        }}>
        <Image
          resizeMethod="resize"
          resizeMode="contain"
          style={styles.uploadedImg}
          source={{uri: item.uri}}
        />
        <TouchableOpacity
          style={styles.uploadImgDeletBtn}
          onPress={() => {
            removeImage(index);
          }}>
          <Remove width={25} height={25} />
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  const onAddProduct = () => {
    let valid = true;

    if (title.length === 0) {
      valid = false;
      setTitleErr('Please enter title.');
    }
    if (description.length === 0) {
      valid = false;
      setDescriptionErr('Please enter description.');
    }
    if (price.length === 0) {
      valid = false;
      setPriceErr('Please enter price.');
    }
    if (discount.length === 0) {
      valid = false;
      setDiscountErr('Please enter discount.');
    } else if (discount > 100) {
      valid = false;
      setDiscountErr('discount should be less then 100%.');
    }
    if (stock.length === 0) {
      valid = false;
      setStockErr('Please enter stock details.');
    }
    if (brand.length === 0) {
      valid = false;
      setBrandErr('Please enter brand name.');
    }
    if (category === null) {
      valid = false;
      setCategoryErr('Please select category.');
    }
    if (rating === null) {
      valid = false;
      setRatingErr('Please select Rating.');
    }
    if (multipleImages.length === 0) {
      valid = false;
      setMultipleImagesErr('Please upload product image.');
    }

    if (valid) {
      let request = {
        title: title,
        description: description,
        price: price,
        discountPercentage: discount,
        stock: stock,
        rating: rating,
        brand: brand,
        category: category,
      };
      if (multipleImages && multipleImages.length > 0) {
        let images = [];
        multipleImages.map((item, index) => {
          images.push(item.uri);
        });
        request.images = images;
      }
      dispatch(addProductAction(request, navigation));
    } else {
      AlertHelper.long('Plese fill proper details to add product.');
    }
  };

  return (
    <SafeAreaView style={[base.block]}>
      <Molecules.AuthHeader
        _onBack={() => {
          navigation.goBack();
        }}
        title={'Add Products'}
      />
      <KeyboardAwareScrollView
        style={[base.horizonalSpace]}
        scrollEnabled={scrollEnabled}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled">
        <Molecules.InputField
          title={'Title'}
          placeholder="Enter Title"
          placeholderTextColor={color._mediumGray}
          value={title}
          error={titleErr}
          onChangeText={text => {
            setTitleErr(null);
            setTitle(text);
          }}
        />

        <Molecules.InputField
          title={'Descripton'}
          // required
          error={descriptionErr}
          value={description}
          placeholder="Enter Description"
          placeholderTextColor={color._mediumGray}
          onChangeText={text => {
            setDescriptionErr(null);
            setDescription(text);
          }}
          textFieldStyle={{
            height: wp(25),
            textAlignVertical: 'top',
          }}
          multiline={true}
        />

        <Molecules.InputField
          title={'Price'}
          placeholder="Enter Price"
          placeholderTextColor={color._mediumGray}
          value={price}
          error={priceErr}
          keyboardType={'number-pad'}
          onChangeText={text => {
            setPriceErr(null);
            let myString = text.replace(/[^0-9]/g, '');
            myString = myString.length > 10 ? price : myString;
            setPrice(myString);
          }}
          isRight
          priceSign={priceUnit}
        />

        <Molecules.InputField
          title={'Discount'}
          placeholder="Enter discount Percentage"
          placeholderTextColor={color._mediumGray}
          value={discount}
          error={discountErr}
          keyboardType={'number-pad'}
          onChangeText={text => {
            setDiscountErr(null);
            let myString = text.replace(/[^0-9.]/g, '');
            myString = myString.length > 10 ? discount : myString;
            setdiscount(myString);
          }}
          isRight
          priceSign={discountUnit}
        />

        <Molecules.InputField
          title={'Stock'}
          placeholder="Enter Available Stock"
          placeholderTextColor={color._mediumGray}
          value={stock}
          error={stockErr}
          keyboardType={'number-pad'}
          onChangeText={text => {
            setStockErr(null);
            let myString = text.replace(/[^0-9]/g, '');
            myString = myString.length > 10 ? stock : myString;
            setStock(myString);
          }}
        />

        <Molecules.InputField
          title={'Brand'}
          placeholder="Enter Brand name"
          placeholderTextColor={color._mediumGray}
          value={brand}
          error={brandErr}
          onChangeText={text => {
            setBrandErr(null);
            setBrand(text);
          }}
        />

        <Atoms.Title title={'Category'} />
        <Molecules.Dropdown
          setShowList={() => {
            Keyboard.dismiss();
            setScrollEnabled(!scrollEnabled);
          }}
          selectedValue={category}
          data={categoryList}
          placeholder={'Select Category'}
          value={category}
          onChangeText={item => {
            setCategory(item);
            setCategoryErr(null);
          }}
        />
        {categoryErr && <Atoms.ErrorText title={categoryErr} />}

        <View style={{alignItems: 'flex-start'}}>
          <Atoms.Title title={'Rating'} />
          <AirbnbRating
            count={5}
            defaultRating={0}
            size={wp(5.5)}
            showRating={false}
            onFinishRating={rating => {
              setRatingErr(null);
              setRating(rating);
            }}
          />
          {ratingErr && <Atoms.ErrorText title={ratingErr} />}
        </View>

        <Atoms.Button
          title={'Upload Images'}
          style={{
            backgroundColor: color._green,
            borderRadius: wp(3.25),
            marginTop: wp(2),
          }}
          textStyle={{
            fontSize: wp(4.5),
            fontWeight: '700',
            color: color._blue,
          }}
          onPress={() => {
            setModalVisible(true);
            setMultipleImagesErr(null);
          }}
        />

        {multipleImagesErr && <Atoms.ErrorText title={multipleImagesErr} />}

        {multipleImages.length !== 0 ? (
          <View style={styles.uploadImgContainer}>
            <View>
              <FlatList
                containerStyle={{flex: 1}}
                data={multipleImages}
                renderItem={_renderFlatList}
                extraData={multipleImages.length}
                keyExtractor={(item, index) => {
                  return index.toString();
                }}
                refreshing={refreshing}
                columnWrapperStyle={[
                  styles.columnWrapperStyle,
                  {marginHorizontal: -wp(0.75)},
                ]}
                numColumns={3}
                bounces={false}
                showsHorizontalScrollIndicator={false}
                showsVerticalScrollIndicator={false}
              />
            </View>
          </View>
        ) : null}

        <Atoms.Button
          title={'ADD PRODUCT'}
          style={{
            backgroundColor: color._green,
            borderRadius: wp(3.25),
            marginVertical: wp(2),
          }}
          textStyle={{
            fontSize: wp(4.5),
            fontWeight: '700',
            color: color._blue,
          }}
          onPress={() => {
            onAddProduct();
          }}
        />
      </KeyboardAwareScrollView>

      <Models.ImagePreview
        isVisible={preview}
        onRequestClose={() => {
          setPreview(false);
        }}
        imageUrl={image}
      />

      <Molecules.ImagePicker
        isVisible={modalVisible}
        chooseCameraImage={() => {
          setModalVisible(false),
            setTimeout(() => {
              chooseCameraImage();
            }, 500);
        }}
        chooseGalleryImage={() => {
          setModalVisible(false),
            setTimeout(() => {
              chooseGalleryImage();
            }, 500);
        }}
        onRequestClose={() => {
          setModalVisible(false);
        }}
        onDelete={() => {
          setModalVisible(false);
        }}
      />
    </SafeAreaView>
  );
};

export default AddProduct;
