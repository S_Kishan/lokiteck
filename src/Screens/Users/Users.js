import {
  View,
  Text,
  SafeAreaView,
  FlatList,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import base from '../../Constants/CommonStyle';
import * as Molecules from '../../Components/molecules';
import Search from '../../assets/icn_search.svg';
import color from '../../Constants/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import styles from './UsersStyle';
import {getAllUsers, getSearchUsers, resetAddStatus} from '../../Store/user';

const Users = ({navigation}) => {
  const dispatch = useDispatch();

  const stateUser = useSelector(state => state.user);

  const [users, setUsers] = React.useState([]);

  const [searchTxt, setSearchTxt] = React.useState('');

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', e => {
      if (stateUser) {
        if (stateUser.addSuccess && stateUser.addedUser) {
          setUsers([stateUser.addedUser, ...users]);
          dispatch(resetAddStatus());
        } else {
          let request = {};
          dispatch(getAllUsers(request, navigation));
        }
      }
    });
    return unsubscribe;
  });

  React.useEffect(() => {
    if (stateUser) {
      if (stateUser.userList) {
        setUsers(stateUser.userList);
      }
    }
  }, [stateUser.userList]);

  const renderUser = ({item, index}) => {
    return (
      <Molecules.UserBlock
        details={item}
        onLocation={() => {
          navigation.navigate('Map', {
            coordinates: item.address.coordinates,
            address: item.address,
          });
        }}
      />
    );
  };

  return (
    <SafeAreaView style={[base.block]}>
      <Molecules.AuthHeader
        back={false}
        _onBack={() => {}}
        title={'Users'}
        btnTitle={'Add User'}
        _onRightBtnPress={() => {
          navigation.navigate('AddUser');
        }}
      />
      <View style={[base.block, base.horizonalSpace]}>
        <Molecules.InputField
          inputContainerStyle={{
            marginTop: wp(2.75),
            height: wp(11),
          }}
          placeholder="Search User"
          placeholderTextColor={color._mediumGray}
          value={searchTxt}
          onChangeText={text => {
            setSearchTxt(text);
          }}
          isRight
          rightContent={
            <TouchableOpacity
              style={[
                base.center,
                {
                  flex: 1,
                  marginLeft: -wp(13),
                  backgroundColor: color._lightGray,
                  borderRadius: wp(2.5),
                },
              ]}
              onPress={() => {
                let request = {
                  query: searchTxt,
                };
                dispatch(getSearchUsers(request, navigation));
              }}>
              <Search height={wp(9)} width={wp(9)} />
            </TouchableOpacity>
          }
        />

        {users.length > 0 ? (
          <FlatList
            data={users}
            keyExtractor={(item, index) => {
              return index.toString();
            }}
            renderItem={renderUser}
          />
        ) : (
          <View style={[base.block, base.center]}>
            <Text style={[base.fontMedium]}>No Users Found.</Text>
          </View>
        )}
      </View>
    </SafeAreaView>
  );
};

export default Users;
