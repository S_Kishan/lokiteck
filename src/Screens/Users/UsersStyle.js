import {StyleSheet, Dimensions} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import color from '../../Constants/Color';

const WIDTH = Dimensions.get('window').width;

const styles = StyleSheet.create({
  uploadImgCol: {
    width: WIDTH / 3 - wp(5.25),
    height: wp(35),
    marginTop: wp(2),
    backgroundColor: color._white,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: wp(2.5),
    margin: wp(0.75),
    borderWidth: 1,
    borderColor: color._mediumGray,
  },
  uploadedImg: {
    width: '97%',
    height: '97%',
  },
  uploadImgDeletBtn: {
    position: 'absolute',
    top: wp(0.5),
    right: wp(0.5),
    zIndex: 1200,
  },
  uploadDeletBtnIcon: {
    fontSize: wp(5),
    backgroundColor: color._black,
    color: color._white,
    borderRadius: 100,
  },
  uploadImgContainer: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: wp(2.5),
    borderWidth: 1,
    borderColor: color._mediumGray,
    borderRadius: wp(2.5),
    paddingBottom: wp(2),
    paddingHorizontal: wp(2),
    minHeight: wp(25),
  },
  photoPlaceholderTxt: {
    fontSize: wp(3.5),
    color: color._black,
  },
  photoBtnTxt: {
    fontSize: wp(4),
    color: color._white,
  },
  photoBtnTxt2: {
    fontSize: wp(2.75),
    color: color._white,
    marginLeft: wp(1.25),
  },

  countryPicker: {
    flex: 1,
    paddingLeft: wp(2.25),
  },

  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

export default styles;
