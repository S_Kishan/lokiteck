import React from 'react';
import {SafeAreaView} from 'react-native';
import {View} from 'react-native';
import MapView, {Marker} from 'react-native-maps';
import base from '../../Constants/CommonStyle';
import * as Molecules from '../../Components/molecules';
import styles from './UsersStyle';

const Map = ({route, navigation}) => {
  const [initialRegion, setInitialregion] = React.useState({
    latitude: 37.78825,
    longitude: -122.4324,
    latitudeDelta: 0.0922,
    longitudeDelta: 0.0421,
  });

  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', e => {
      if (route && route.params) {
        if (route.params.coordinates) {
          setInitialregion({
            latitude: route.params.coordinates.lat,
            longitude: route.params.coordinates.lng,
            latitudeDelta: 0.155,
            longitudeDelta: 0.155,
          });
        }
      }
    });
    return unsubscribe;
  });

  return (
    <SafeAreaView style={[base.block]}>
      <Molecules.AuthHeader
        _onBack={() => {
          navigation.goBack();
        }}
        title={'Map'}
      />
      <View style={[base.block, base.center]}>
        <MapView style={styles.map} initialRegion={initialRegion}>
          {route &&
            route.params &&
            route.params.coordinates &&
            route.params.coordinates.lat &&
            route.params.coordinates.lng && (
              <Marker
                coordinate={{
                  latitude: route.params.coordinates.lat,
                  longitude: route.params.coordinates.lng,
                }}
                title={
                  route &&
                  route.params &&
                  route.params.address &&
                  route.params.address.address &&
                  route.params.address.address
                }
                description={
                  route &&
                  route.params &&
                  route.params.address &&
                  route.params.address.city &&
                  route.params.address.city
                }
              />
            )}
        </MapView>
      </View>
    </SafeAreaView>
  );
};

export default Map;
