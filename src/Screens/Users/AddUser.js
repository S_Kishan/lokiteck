import {SafeAreaView, Keyboard} from 'react-native';
import React from 'react';
import base from '../../Constants/CommonStyle';
import styles from './UsersStyle';
import * as Molecules from '../../Components/molecules';
import * as Atoms from '../../Components/atoms';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import color from '../../Constants/Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import {useDispatch} from 'react-redux';
import {AlertHelper} from '../../Constants/AlertHelper';
import {isValidEmail} from '../../Constants/Validation';
import {addUserAction} from '../../Store/user';

const AddUser = ({navigation}) => {
  const dispatch = useDispatch();

  const [first, setFirst] = React.useState('');
  const [firstErr, setFirstErr] = React.useState(null);

  const [last, setLast] = React.useState('');
  const [lastErr, setLastErr] = React.useState(null);

  const [age, setAge] = React.useState('');
  const [ageErr, setAgeErr] = React.useState(null);

  const [genderList, setGenderList] = React.useState(['Male', 'Female']);
  const [gender, setGender] = React.useState(null);
  const [genderErr, setGenderErr] = React.useState(null);

  const [email, setEmail] = React.useState('');
  const [emailErr, setEmailErr] = React.useState(null);
  const [emailValid, setEmailValid] = React.useState(2);

  const [mobile, setMobile] = React.useState('');
  const [mobileErr, setMobileErr] = React.useState(null);

  const [scrollEnabled, setScrollEnabled] = React.useState(true);

  const _emailValid = email => {
    if (email.length !== 0) {
      if (isValidEmail(email)) {
        setEmailErr(null);
      } else {
        setEmailErr('Please enter valid email.');
      }
      isValidEmail(email) === false && setEmailValid(0);
      isValidEmail(email) === true && setEmailValid(1);
    }
  };

  const onAddUser = () => {
    let valid = true;

    if (first.length === 0) {
      valid = false;
      setFirstErr('Please enter First Name.');
    }
    if (last.length === 0) {
      valid = false;
      setLastErr('Please enter Last Name.');
    }
    if (age.length === 0) {
      valid = false;
      setAgeErr('Please enter age.');
    }
    if (mobile.length === 0) {
      valid = false;
      setMobileErr('Please enter Mobile Number.');
    } else if (mobile.length < 10) {
      valid = false;
      setMobileErr('Mobile number shuld be atleast 10 digit.');
    }
    if (email.length === 0) {
      valid = false;
      setEmailErr('Please enter email.');
    }
    if (gender === null) {
      valid = false;
      setGenderErr('Please select gender.');
    }

    if (valid) {
      let request = {
        firstName: first,
        lastName: last,
        age: age,
        gender: gender,
        email: email,
        phone: mobile,
      };
      dispatch(addUserAction(request, navigation));
    } else {
      AlertHelper.long('Plese fill proper details to add user.');
    }
  };

  return (
    <SafeAreaView style={[base.block]}>
      <Molecules.AuthHeader
        _onBack={() => {
          navigation.goBack();
        }}
        title={'Add User'}
      />
      <KeyboardAwareScrollView
        style={[base.horizonalSpace]}
        scrollEnabled={scrollEnabled}
        showsVerticalScrollIndicator={false}
        keyboardShouldPersistTaps="handled">
        <Molecules.InputField
          title={'First Name'}
          placeholder="Enter First Name"
          placeholderTextColor={color._mediumGray}
          value={first}
          error={firstErr}
          onChangeText={text => {
            setFirstErr(null);
            setFirst(text);
          }}
        />
        <Molecules.InputField
          title={'Last Name'}
          placeholder="Enter Last Name"
          placeholderTextColor={color._mediumGray}
          value={last}
          error={lastErr}
          onChangeText={text => {
            setLastErr(null);
            setLast(text);
          }}
        />

        <Molecules.InputField
          title={'Age'}
          placeholder="Enter age"
          placeholderTextColor={color._mediumGray}
          value={age}
          error={ageErr}
          keyboardType={'number-pad'}
          onChangeText={text => {
            setAgeErr(null);
            let myString = text.replace(/[^0-9]/g, '');
            setAge(myString);
          }}
        />

        <Atoms.Title title={'Gender'} />
        <Molecules.Dropdown
          setShowList={() => {
            Keyboard.dismiss();
            setScrollEnabled(!scrollEnabled);
          }}
          selectedValue={gender}
          data={genderList}
          placeholder={'Select Gender'}
          value={gender}
          onChangeText={item => {
            setGender(item);
            setGenderErr(null);
          }}
        />
        {genderErr && <Atoms.ErrorText title={genderErr} />}

        <Molecules.InputField
          title={'Email'}
          placeholder="Enter Email"
          placeholderTextColor={color._mediumGray}
          required
          value={email}
          error={emailErr}
          isRight={emailValid == 2 ? false : true}
          errorEmail
          iconStyle={{
            marginTop: -wp(1),
            justifyContent: 'center',
            alignItems: 'center',
          }}
          keyboardType={'email-address'}
          onChangeText={text => {
            _emailValid(text);
            setEmail(text);
          }}
          autoCapitalize={'none'}
        />

        <Molecules.InputField
          title={'Mobile'}
          placeholder="Enter Mobile Number"
          placeholderTextColor={color._mediumGray}
          value={mobile}
          error={mobileErr}
          keyboardType={'number-pad'}
          onChangeText={text => {
            setMobileErr(null);
            let myString = text.replace(/[^0-9]/g, '');
            myString = myString.length > 10 ? mobile : myString;
            setMobile(myString);
          }}
          length={10}
        />

        <Atoms.Button
          title={'ADD USER'}
          style={{
            backgroundColor: color._green,
            borderRadius: wp(3.25),
            marginVertical: wp(2),
          }}
          textStyle={{
            fontSize: wp(4.5),
            fontWeight: '700',
            color: color._blue,
          }}
          onPress={() => {
            onAddUser();
          }}
        />
      </KeyboardAwareScrollView>
    </SafeAreaView>
  );
};

export default AddUser;
