import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  container: {
    borderRadius: 3,
    paddingVertical: wp(3.5),
  },
  submitBtnContainer: {
    width: '100%',
  },
  bottomBtn: {
    borderRadius: 3,
    paddingVertical: wp(3),
    justifyContent: 'flex-end',
    alignContent: 'space-between',
    position: 'absolute',
    bottom: wp(2),
    width: '100%',
  },
  btnText: {
    fontSize: wp(4),
    textAlign: 'center',
    fontWeight: '700',
  },
});
export default styles;
