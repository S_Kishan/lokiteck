import React from 'react';
import {TextInput, Image} from 'react-native';
import styles from './TextInputStyle';

const TextField = ({
  value,
  textFieldStyle,
  multiline,
  onSubmitEditing,
  placeholder,
  placeholderTextColor,
  onChangeText,
  keyboardType,
  editable,
  onFocus,
  onRef,
  secureTextEntry,
  error,
  onBlur,
  disable,
  length,
  ...props
}) => (
  <TextInput
    maxLength={length}
    keyboardType={keyboardType ? keyboardType : 'default'}
    secureTextEntry={secureTextEntry ? secureTextEntry : false}
    style={[
      styles.textFieldDefault,
      error ? {color: 'red'} : {},
      textFieldStyle,
    ]}
    underlineColorAndroid={'transparent'}
    onChangeText={text => {
      onChangeText(text);
    }}
    value={value}
    editable={editable}
    placeholder={placeholder}
    onSubmitEditing={() => onSubmitEditing}
    placeholderTextColor={placeholderTextColor}
    ref={onRef != undefined ? onRef : null}
    onFocus={() => {
      if (onFocus !== undefined) {
        onFocus();
      }
    }}
    onBlur={() => {
      if (onBlur !== undefined) {
        onBlur();
      }
    }}
    multiline={multiline}
    {...props}
  />
);

export default TextField;
