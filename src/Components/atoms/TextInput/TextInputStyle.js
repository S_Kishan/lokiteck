import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  textFieldDefault: {
    height: wp(10),
    width: '100%',
    fontSize: wp(3.5),
  },
});
export default styles;
