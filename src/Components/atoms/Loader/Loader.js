import React from 'react';
import {StyleSheet, View, Modal, ActivityIndicator} from 'react-native';

const Loader = props => {
  const {loading} = props;
  return (
    <Modal visible={loading} transparent={true} animationType={'none'}>
      <View
        style={{
          backgroundColor: 'rgba(0,0,0,0.2)',
          flex: 1,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <ActivityIndicator size="large" color="#0000ff" />
      </View>
    </Modal>
  );
};
const styles = StyleSheet.create({});
export default Loader;
