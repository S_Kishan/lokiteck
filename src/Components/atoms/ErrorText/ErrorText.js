import React from 'react';
import {View, Text} from 'react-native';
import color from '../../../Constants/Color';
import base from '../../../Constants/CommonStyle';
import styles from './ErrorTextStyle';

const ErrorText = ({style, textStyle, title, ...props}) => (
  <View style={[styles.Container, style]} {...props}>
    <Text style={[base.hilitedFont, {color: color._red, marginTop: 7}]}>
      {title}
    </Text>
  </View>
);

export default ErrorText;
