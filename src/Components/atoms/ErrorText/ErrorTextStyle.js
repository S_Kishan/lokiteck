import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import color from '../../../Constants/Color';

const styles = StyleSheet.create({
  Container: {},
  titleTxt: {
    fontSize: wp(2),
    fontWeight: '700',
    marginBottom: wp(1),
    color: color._red,
  },
  inputLableTaxt: {
    fontSize: wp(4),
    fontWeight: '700',
    marginBottom: wp(2),
  },
});
export default styles;
