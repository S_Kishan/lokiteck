import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import color from '../../../Constants/Color';

const styles = StyleSheet.create({
  Container: {},
  titleTxt: {
    fontSize: wp(4),
    fontWeight: '700',
    marginTop: wp(2),
    color: color._black,
  },
  inputLableTaxt: {
    fontSize: wp(4),
    fontWeight: '700',
    marginTop: wp(2),
  },
});
export default styles;
