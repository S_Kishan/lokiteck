import React, {useState} from 'react';
import {Text, View, Image, ActivityIndicator} from 'react-native';
import styles from './ProductBlockStyle';
import base from '../../../Constants/CommonStyle';
import color from '../../../Constants/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Star from '../../../assets/icn_star.svg';

const ProductBlock = props => {
  const {details} = props;

  const [loaded, setLoaded] = useState(false);

  return (
    <View
      style={[
        {
          backgroundColor: '#fff',
          shadowColor: '#000000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.17,
          shadowRadius: 2.54,
          elevation: 3,
          padding: wp(5),
          borderRadius: wp(2.5),
          marginVertical: wp(1.5),
        },
      ]}>
      <View style={[{flexDirection: 'row'}]}>
        <View
          style={{
            height: wp(23.5),
            width: wp(22.5),
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: color._mediumGray,
            borderRadius: wp(2.5),
            marginRight: wp(3.5),
          }}>
          {!loaded && (
            <View
              style={{
                height: wp(23.5),
                width: wp(22.5),
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: color._mediumGray,
                borderRadius: wp(2.5),
                marginRight: wp(3.5),
              }}>
              <ActivityIndicator size="large" color="white" />
            </View>
          )}
          <Image
            source={{uri: details.images[0]}}
            style={{
              height: wp(23.5),
              width: wp(22.5),
              backgroundColor: color._mediumGray,
              borderRadius: wp(2.5),
              marginRight: wp(3.5),
              position: 'absolute',
            }}
            onLoad={() => setLoaded(true)}
            resizeMethod="resize"
            resizeMode="contain"
          />
        </View>
        <View style={[base.block]}>
          <View style={[base.horizontal, {justifyContent: 'space-between'}]}>
            <Text
              style={[
                base.fontBold,
                {color: color._black, flex: 1, marginRight: wp(1.25)},
              ]}
              numberOfLines={2}>
              {details.title}
            </Text>
            <View style={[styles.brandBlock]}>
              <Text
                style={[
                  base.fontSmall,
                  {fontSize: wp(3.5), color: color._white},
                ]}
                numberOfLines={1}>
                {details.brand}
              </Text>
            </View>
          </View>
          <Text
            style={[base.descriptionTxt, {marginVertical: wp(0.75)}]}
            numberOfLines={3}>
            {details.description}
          </Text>
          <Text
            style={[base.fontSmall, {fontSize: wp(3.5), color: color._gray}]}>
            Category : {details.category}
          </Text>

          <View
            style={[
              base.horizontal,
              {justifyContent: 'space-between', marginTop: wp(1.25)},
            ]}>
            <Text
              style={[
                {fontSize: wp(4), fontWeight: '700', color: color._black},
              ]}>
              ${details.price}
            </Text>
            <View style={[base.horizontal]}>
              <Star height={wp(4.5)} width={wp(4.5)} />
              <Text
                style={[
                  {fontSize: wp(3.5), fontWeight: '500', color: color._black},
                ]}>
                {details.rating}
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default ProductBlock;
