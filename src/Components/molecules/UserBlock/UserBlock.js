import React from 'react';
import {
  TouchableOpacity,
  Text,
  View,
  Image,
  ActivityIndicator,
} from 'react-native';
import styles from './UserBlockStyle';
import base from '../../../Constants/CommonStyle';
import color from '../../../Constants/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';
import Location from '../../../assets/icn_location.svg';
import * as Atoms from '../../atoms';

const UserBlock = props => {
  const {details, onLocation} = props;

  const [loaded, setLoaded] = React.useState(false);

  return (
    <View
      style={[
        {
          backgroundColor: '#fff',
          shadowColor: '#000000',
          shadowOffset: {
            width: 0,
            height: 2,
          },
          shadowOpacity: 0.17,
          shadowRadius: 2.54,
          elevation: 3,
          padding: wp(5),
          borderRadius: wp(2.5),
          marginVertical: wp(1.5),
        },
      ]}>
      <View style={[{flexDirection: 'row'}]}>
        <View
          style={{
            height: wp(23.5),
            width: wp(22.5),
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: color._mediumGray,
            borderRadius: wp(2.5),
            marginRight: wp(3.5),
          }}>
          {!loaded && (
            <View
              style={{
                height: wp(23.5),
                width: wp(22.5),
                justifyContent: 'center',
                alignItems: 'center',
                backgroundColor: color._mediumGray,
                borderRadius: wp(2.5),
                marginRight: wp(3.5),
              }}>
              <ActivityIndicator size="large" color="white" />
            </View>
          )}
          {details.image && (
            <Image
              source={{uri: details.image}}
              style={{
                height: wp(23.5),
                width: wp(22.5),
                backgroundColor: color._mediumGray,
                borderRadius: wp(2.5),
                marginRight: wp(3.5),
                position: 'absolute',
              }}
              onLoad={() => setLoaded(true)}
              resizeMethod="resize"
              resizeMode="contain"
            />
          )}
        </View>
        <View style={[base.block]}>
          <View style={[base.horizontal, {justifyContent: 'space-between'}]}>
            {details.firstName && details.lastName && (
              <Text
                style={[
                  base.fontBold,
                  {color: color._black, flex: 1, marginRight: wp(1.25)},
                ]}
                numberOfLines={2}>
                {details.firstName + ' ' + details.lastName}
              </Text>
            )}
            {details &&
              details.address &&
              details.address.coordinates &&
              details.address.coordinates.lat !== null &&
              details.address.coordinates.lng !== null && (
                <TouchableOpacity
                  style={[styles.brandBlock]}
                  onPress={() => {
                    onLocation();
                  }}>
                  <Location height={wp(6)} width={wp(6)} />
                </TouchableOpacity>
              )}
          </View>
          {details.email && (
            <Text
              style={[base.descriptionTxt, {marginVertical: wp(0.75)}]}
              numberOfLines={1}>
              {details.email}
            </Text>
          )}
          {details.phone && (
            <Text
              style={[base.descriptionTxt, {marginVertical: wp(0.75)}]}
              numberOfLines={1}>
              {details.phone}
            </Text>
          )}
          {details.birthDate && (
            <Text
              style={[base.fontSmall, {fontSize: wp(3.5), color: color._gray}]}>
              DOB : {details.birthDate}
            </Text>
          )}
        </View>
      </View>
      <View
        style={[
          base.horizontal,
          {justifyContent: 'space-between', marginTop: wp(1.25)},
        ]}>
        {details.age && (
          <Text style={[styles.descText]}>Age : {details.age}</Text>
        )}
        {details.gender && (
          <Text style={[styles.descText]}>Gender : {details.gender}</Text>
        )}
      </View>
      <View
        style={[
          base.horizontal,
          {justifyContent: 'space-between', marginTop: wp(1.25)},
        ]}>
        {details.height && (
          <Text style={[styles.descText]}>Height : {details.height}</Text>
        )}
        {details.weight && (
          <Text style={[styles.descText]}>Weight : {details.weight}</Text>
        )}
      </View>
      {details.hair && details.hair.color && details.hair.type && (
        <>
          <View style={[base.deviderLine]} />
          <Atoms.Title title={'Hair'} />
          <View
            style={[
              base.horizontal,
              {justifyContent: 'space-between', marginTop: wp(1.25)},
            ]}>
            <Text style={[styles.descText]}>Color : {details.hair.color}</Text>
            <Text style={[styles.descText]}>Type : {details.hair.type}</Text>
          </View>
        </>
      )}
      {details.address && details.address.address && details.address.city && (
        <>
          <View style={[base.deviderLine]} />
          <Atoms.Title title={'Address'} />
          <Text style={[styles.descText, {marginTop: wp(1.25)}]}>
            Address : {details.address.address}
          </Text>
          <Text style={[styles.descText, {marginTop: wp(1.25)}]}>
            City : {details.address.city}
          </Text>
          <Text style={[styles.descText, {marginTop: wp(1.25)}]}>
            Post Code : {details.address.postalCode}
          </Text>
        </>
      )}
      {details.company &&
        details.company.address.address &&
        details.company.address.city && (
          <>
            <View style={[base.deviderLine]} />
            <Atoms.Title title={'Company Address'} />
            <Text style={[styles.descText, {marginTop: wp(1.25)}]}>
              Address : {details.company.address.address}
            </Text>
            <Text style={[styles.descText, {marginTop: wp(1.25)}]}>
              City : {details.company.address.city}
            </Text>
            <Text style={[styles.descText, {marginTop: wp(1.25)}]}>
              Post Code : {details.company.address.postalCode}
            </Text>
          </>
        )}
    </View>
  );
};

export default UserBlock;
