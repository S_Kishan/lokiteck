import {StyleSheet} from 'react-native';
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp,
} from 'react-native-responsive-screen';
import color from '../../../Constants/Color';

const styles = StyleSheet.create({
  headerContainer: {
    minHeight: hp(6),
    justifyContent: 'space-between',
    paddingBottom: wp(2.25),
    paddingHorizontal: wp(5),
  },
  backBox: {
    padding: 5,
    borderWidth: 0.5,
    borderRadius: wp(2.25),
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 100,
    marginLeft: wp(5),
    borderColor: color._mediumGray,
  },
  textLogin: {
    color: color._blue,
    fontWeight: '700',
  },
  brandBlock: {
    borderRadius: wp(1.25),
    maxWidth: wp(25),
  },

  descText: {flex: 1, fontSize: wp(4), fontWeight: '700', color: color._black},
});

export default styles;
