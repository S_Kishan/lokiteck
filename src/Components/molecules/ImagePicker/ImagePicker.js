import React from 'react';
import {useSelector} from 'react-redux';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './ImagePickerStyle';
import Modal from 'react-native-modal';
import Camera from '../../../assets/icn_camera.svg';
import Gallary from '../../../assets/icn_gallary.svg';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

export const ImagePicker = props => {
  const [modalVisible, setModalVisible] = React.useState(false);

  const {
    isVisible,
    onRequestClose,
    chooseGalleryImage,
    chooseCameraImage,
    onRef,
  } = props;

  React.useEffect(() => {
    setModalVisible(isVisible);
  }, [isVisible]);

  return (
    <View>
      <Modal
        ref={onRef != undefined ? onRef : null}
        isVisible={modalVisible}
        onBackButtonPress={() => {
          onRequestClose();
        }}
        onBackdropPress={() => {
          onRequestClose();
        }}
        animationIn="slideInUp"
        animationOut="slideOutDown"
        style={{}}>
        <View style={styles.addressModalContainer}>
          <View style={styles.addressModalbody}>
            <View style={styles.addressModalCard}>
              <View style={styles.addressModalCardHeader}>
                <Text style={{color: 'black', fontSize: wp(3.25)}}>
                  Upload Image
                </Text>
              </View>
              <TouchableOpacity
                style={[styles.btn1, styles.middleBtn]}
                onPress={() => {
                  chooseCameraImage();
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Camera width={wp(6)} height={wp(6)} />
                  <Text style={styles.btn1Txt}>Open Camera</Text>
                </View>
              </TouchableOpacity>
              <TouchableOpacity
                style={styles.btn1}
                onPress={() => {
                  chooseGalleryImage();
                }}>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                  }}>
                  <Gallary width={wp(6)} height={wp(6)} />
                  <Text style={styles.btn1Txt}>Select From Gellery</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </View>
  );
};

export default ImagePicker;
