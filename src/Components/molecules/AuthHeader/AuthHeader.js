import React from 'react';
import {TouchableOpacity, Text, View, Dimensions} from 'react-native';
import styles from './AuthHeaderStyle';
import Left from '../../../assets/ic_left_arrow.svg';
import base from '../../../Constants/CommonStyle';
import color from '../../../Constants/Color';
import {widthPercentageToDP as wp} from 'react-native-responsive-screen';

const AuthHeader = props => {
  const {
    back = true,
    _onBack,
    title,
    btnTitle,
    _onRightBtnPress,
    disabled = false,
  } = props;
  return (
    <View
      style={[
        styles.headerContainer,
        base.horizontal,
        {backgroundColor: color._white},
      ]}>
      {back && (
        <TouchableOpacity
          style={[styles.backBox, {position: 'absolute'}]}
          onPress={() => {
            _onBack();
          }}>
          <Left width={wp(3)} height={wp(3)} />
        </TouchableOpacity>
      )}
      {title && (
        <View style={[base.block, base.center]}>
          <Text style={[base.fontMedium, {color: color._black, padding: 4}]}>
            {title}
          </Text>
        </View>
      )}
      {btnTitle && (
        <TouchableOpacity
          style={[
            {
              position: 'absolute',
              right: wp(5),
              top: Dimensions.get('window').width > 400 ? wp(1) : wp(2.5),
            },
          ]}
          disabled={disabled}
          onPress={() => {
            _onRightBtnPress();
          }}>
          <Text
            style={[
              base.fontMedium,
              styles.textLogin,
              {color: disabled ? color._gray : color._blue},
            ]}>
            {btnTitle}
          </Text>
        </TouchableOpacity>
      )}
    </View>
  );
};

export default AuthHeader;
