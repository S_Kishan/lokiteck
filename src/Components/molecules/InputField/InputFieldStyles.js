import {StyleSheet, Dimensions, StatusBar} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import color from '../../../Constants/Color';

const width = Dimensions.get('window').width;
const styles = StyleSheet.create({
  inputBoxContainer: {
    marginBottom: wp(2),
  },
  inputLableTaxt: {
    fontSize: wp(4),
    fontWeight: '700',
    marginBottom: wp(2),
    color: color._black,
  },
  InputRow: {
    borderRadius: 3,
    flexDirection: 'row',
  },
  inputContainer: {
    minHeight: wp(11),
    paddingHorizontal: wp(3.5),
    textAlignVertical: 'center',
    color: '#000',
    borderColor: color._mediumGray,
    borderWidth: 1,
    borderRadius: wp(2.5),
    backgroundColor: color._white,
  },
  iconRowpass: {
    position: 'absolute',
    right: wp(0),
    height: wp(10),
    width: wp(10),
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  leftIcon: {
    justifyContent: 'center',
  },
  icon: {
    fontSize: 20,
    textAlignVertical: 'center',
    textAlign: 'center',
  },
  rightIcon: {
    fontSize: 20,
  },
  leftContent: {},
  rightContent: {},
});
export default styles;
