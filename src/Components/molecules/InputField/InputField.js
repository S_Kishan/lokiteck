import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import styles from './InputFieldStyles';
import * as Atoms from '../../atoms';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import color from '../../../Constants/Color';
import Done from '../../../assets/ic_done.svg';
import Wrong from '../../../assets/icn_wrong.svg';
import Eye from '../../../assets/icn_eye.svg';
import EyeActive from '../../../assets/icn_eyeactive.svg';
import base from '../../../Constants/CommonStyle';

export const InputField = props => {
  const [isVisible, setIsVisible] = React.useState(false);
  const [iconColor, setIconColor] = React.useState(color._mediumGray);

  const {
    title,
    mainContainerStyle,
    multiline,
    inputContainerStyle,
    titleStyle,
    value,
    iconStyle,
    textFieldStyle,
    placeholder,
    onChangeText,
    keyboardType,
    readOnly,
    onFocus,
    onRef,
    secureTextEntry,
    error,
    onBlur,
    disable,
    length,
    priceSign,
    rightPriceSign,
    leftContent,
    leftContentStyle,
    placeholderTextColor,
    deviderLine,
    rightContent,
    rightContentStyle,
  } = props;
  React.useEffect(() => {
    if (secureTextEntry) {
      setIsVisible(true);
    }
  }, [secureTextEntry]);
  const _changeIcon = () => {
    setIconColor(
      iconColor === color._mediumGray ? color._blue : color._mediumGray,
    );
    setIsVisible(!isVisible);
  };
  return (
    <View style={[styles.inputBoxContainer, mainContainerStyle]}>
      {title && (
        <Text style={[styles.inputLableTaxt, titleStyle]}>{title}</Text>
      )}
      <View style={[styles.InputRow, inputContainerStyle]}>
        {props.leftContent && (
          <View style={[styles.leftContent, leftContentStyle]}>
            {leftContent}
          </View>
        )}
        {deviderLine && (
          <View
            style={{
              width: 1,
              height: '75%',
              backgroundColor: color._mediumGray,
              marginTop: wp(1.25),
            }}
          />
        )}
        <View
          style={{
            width: props.secureTextEntry ? '100%' : '100%',
            alignItems: 'center',
          }}>
          <Atoms.TextInput
            textFieldStyle={[styles.inputContainer, textFieldStyle]}
            placeholder={placeholder}
            placeholderTextColor={placeholderTextColor}
            value={value}
            onChangeText={text => onChangeText(text)}
            onFocus={() => {
              if (onFocus !== undefined) {
                onFocus();
              }
            }}
            onBlur={() => {
              if (onBlur !== undefined) {
                onBlur();
              }
            }}
            length={length}
            error={error}
            secureTextEntry={isVisible ? true : false}
            onRef={onRef}
            editable={readOnly ? false : true}
            keyboardType={keyboardType}
            disable={disable}
            multiline={multiline}
          />
        </View>
        {props.isRight && priceSign && (
          <View style={[styles.iconRowpass, iconStyle]}>
            <Text
              style={[
                rightPriceSign,
                {
                  justifyContent: 'center',
                  alignItems: 'center',
                  paddingVertical: wp(0),
                  margin: 0,
                  fontSize: wp(5),
                  fontWeight: '700',
                  color: color._gray,
                },
              ]}>
              {priceSign}
            </Text>
          </View>
        )}
        {props.rightContent && (
          <View style={[styles.rightContent, rightContentStyle]}>
            {rightContent}
          </View>
        )}
        {props.errorEmail && props.isRight && (
          <TouchableOpacity
            disabled
            style={[styles.iconRowpass, iconStyle, {marginTop: wp(0.25)}]}>
            {props.done == false && <Wrong />}
            {props.done == true && <Done />}
          </TouchableOpacity>
        )}
        {props.secureTextEntry && props.isRight && (
          <TouchableOpacity
            style={[styles.iconRowpass, iconStyle]}
            onPress={() => _changeIcon()}>
            {isVisible ? (
              <Eye height={wp(4.75)} width={wp(4.75)} />
            ) : (
              <EyeActive height={wp(4.75)} width={wp(4.75)} />
            )}
          </TouchableOpacity>
        )}
      </View>
      {props.error ? (
        <Text style={[base.hilitedFont, {color: color._red, marginTop: 7}]}>
          {error}
        </Text>
      ) : null}
    </View>
  );
};

export default InputField;
