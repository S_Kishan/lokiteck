import {StyleSheet} from 'react-native';
import color from '../../../Constants/Color';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';

export default styles = StyleSheet.create({
  drodownContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 11,
    borderWidth: 1,
    borderColor: color._mediumGray,
    borderRadius: wp(2.5),
    marginTop: wp(3.25),
    backgroundColor: color._white,
    minHeight: wp(11),
    marginBottom: wp(2),
  },
  touchableView: {
    width: '100%',
    justifyContent: 'center',
    borderBottomColor: 'lightgray',
    marginLeft: 'auto',
    marginRight: 'auto',
    marginBottom: wp(2.5),
  },
  selectedTxt: {
    fontSize: wp(4.75),
    color: color._black,
  },
  landPlaceHolder: {
    fontSize: wp(3.5),
    color: color._mediumGray,
  },
  downArrowIcon: {
    fontSize: wp(5.5),
    position: 'absolute',
    right: wp(4.5),
  },
  dropdownList: {
    width: '100%',
    maxHeight: wp(30),
    borderColor: color._white,
    borderWidth: 1,
    minHeight: wp(20),
    marginTop: wp(0.5),
    borderRadius: wp(2.5),
    zIndex: 1020,
    overflow: 'hidden',
    elevation: 5,
  },
  dropdownItems: {
    width: '100%',
    height: wp(10),
    backgroundColor: color._white,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomColor: color._lightGray,
    paddingHorizontal: wp(2.5),
    borderBottomWidth: 1,
    zIndex: 1,
    marginBottom: wp(0.25),
  },
  w100: {
    width: '100%',
  },
  checkIcon: {
    fontSize: wp(5),
    color: '#000',
    position: 'absolute',
    right: wp(2.5),
    color: color._white,
  },

  mainContainer: {
    position: 'relative',
  },
  labelTxt: {
    fontSize: wp(3.5),
    color: color._black,
  },
});
