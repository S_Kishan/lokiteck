import React from 'react';
import {TouchableOpacity, Text, View, FlatList} from 'react-native';
import color from '../../../Constants/Color';
import Up from '../../../assets/icn_up.svg';
import Down from '../../../assets/icn_down.svg';
import Check from '../../../assets/ic_check.svg';
import styles from './DropdownStyle';

export const Dropdown = props => {
  const [showList, setShow] = React.useState(false);

  const {
    mainContainer,
    data,
    selectedValue,
    isShowList,
    listItemStyle,
    onChangeText,
    inputContainerStyle,
    listStyle,
    setShowList,
    placeholder,
    placeholderStyle,
  } = props;

  return (
    <View style={[styles.mainContainer, mainContainer]}>
      <TouchableOpacity
        style={[styles.drodownContainer, inputContainerStyle]}
        onPress={() => {
          setShow(!showList), setShowList();
        }}
        {...props}>
        {selectedValue !== null ? (
          <Text style={styles.selectedTxt}>{selectedValue}</Text>
        ) : (
          <Text style={[styles.landPlaceHolder, placeholderStyle]}>
            {placeholder}
          </Text>
        )}

        {showList === false ? (
          <Down height={15} width={15} />
        ) : (
          <Up height={15} width={15} />
        )}
      </TouchableOpacity>
      {(showList === true || isShowList === true) && (
        <View style={[styles.dropdownList, listStyle]}>
          <FlatList
            data={data}
            style={[styles.w100]}
            extraData={data}
            bounces={false}
            showsHorizontalScrollIndicator={false}
            showsVerticalScrollIndicator={false}
            renderItem={({item, index}) => {
              return (
                <TouchableOpacity
                  style={[
                    styles.dropdownItems,
                    {borderBottomWidth: index === data.length - 1 ? 0 : 1},
                    listItemStyle,
                  ]}
                  onPress={() => {
                    setShow(!showList);
                    setShowList();
                    onChangeText(item);
                  }}>
                  {selectedValue != null ? (
                    <>
                      <Text
                        style={[
                          item === selectedValue
                            ? {color: color._mediumGray}
                            : {color: color._white},
                          styles.labelTxt,
                        ]}>
                        {item}
                      </Text>

                      {item === selectedValue && (
                        <Check width={20} height={20} />
                      )}
                    </>
                  ) : (
                    <Text style={[{color: color._white}, styles.labelTxt]}>
                      {item}
                    </Text>
                  )}
                </TouchableOpacity>
              );
            }}
          />
        </View>
      )}
    </View>
  );
};
export default Dropdown;
