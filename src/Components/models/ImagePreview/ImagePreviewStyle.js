import {StyleSheet} from 'react-native';
import {widthPercentageToDP} from 'react-native-responsive-screen';

const styles = StyleSheet.create({
  modal: {
    margin: 0,
  },
  safeView: {
    flex: 1,
  },
  uploadedImg: {
    width: '100%',
    height: '80%',
  },

  uploadImgDeletBtn: {
    paddingHorizontal: widthPercentageToDP(3.5),
    alignItems: 'flex-end',
    marginTop: widthPercentageToDP(3.5),
  },
  activityIndicator: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
});
export default styles;
