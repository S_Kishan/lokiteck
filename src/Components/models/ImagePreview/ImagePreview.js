import React from 'react';
import {
  View,
  TouchableOpacity,
  Image,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import styles from './ImagePreviewStyle';
import Modal from 'react-native-modal';
import base from '../../../Constants/CommonStyle';
import Color from '../../../Constants/Color';
import Remove from '../../../assets/icn_remove_image.svg';
import {widthPercentageToDP} from 'react-native-responsive-screen';

export const ImagePreview = props => {
  const [modalVisible, setModalVisible] = React.useState(false);

  const {isVisible, onRequestClose, onRef, imageUrl} = props;

  const [loading, setLoading] = React.useState(true);

  React.useEffect(() => {
    setModalVisible(isVisible);
  }, [isVisible]);

  return (
    <View>
      <Modal
        ref={onRef != undefined ? onRef : null}
        isVisible={modalVisible}
        onBackButtonPress={() => {
          onRequestClose();
        }}
        onBackdropPress={() => {
          onRequestClose();
        }}
        animationIn="fadeInUp"
        animationOut="fadeInUp"
        style={styles.modal}>
        <SafeAreaView style={[styles.safeView]}>
          <View style={styles.uploadImgDeletBtn}>
            <TouchableOpacity
              onPress={() => {
                onRequestClose();
              }}>
              <Remove
                width={widthPercentageToDP(6)}
                height={widthPercentageToDP(6)}
              />
            </TouchableOpacity>
          </View>

          <View style={[base.center, styles.safeView]}>
            {imageUrl && (
              <Image
                onLoadStart={() => setLoading(true)}
                onLoadEnd={() => setLoading(false)}
                resizeMode="contain"
                style={styles.uploadedImg}
                source={{uri: imageUrl}}
              />
            )}
            <ActivityIndicator
              style={styles.activityIndicator}
              animating={loading}
              color={Color._blue}
              size={'large'}
            />
          </View>
        </SafeAreaView>
      </Modal>
    </View>
  );
};

export default ImagePreview;
