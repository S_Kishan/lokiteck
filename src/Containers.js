import React, {useEffect} from 'react';
import {
  BackHandler,
  KeyboardAvoidingView,
  SafeAreaView,
  StatusBar,
  useColorScheme,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import color from './Constants/Color';
import base from './Constants/CommonStyle';
import Navigation from './Navigation';
import {setCurrentRouteName} from './Store/global';
import * as Atoms from './Components/atoms';

const Containers = ({navigation}) => {
  const dispatch = useDispatch();
  const stateGlobal = useSelector(state => state.global);

  const isDarkMode = useColorScheme() === 'dark';

  const backHandlerListener = value => {
    if (
      stateGlobal.currentRouteName == 'Home' ||
      stateGlobal.currentRouteName == 'Products'
    ) {
      return true;
    } else if (
      stateGlobal.currentRouteName == 'Login' ||
      stateGlobal.currentRouteName == 'Products'
    ) {
      return true;
    } else {
      BackHandler.removeEventListener('hardwareBackPress', backHandlerListener);
      return false;
    }
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', backHandlerListener);
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', backHandlerListener);
    };
  }, [backHandlerListener]);

  return (
    <>
      <StatusBar
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
        backgroundColor={isDarkMode ? color._white : color._white}
      />
      <SafeAreaView style={{flex: 1}}>
        <KeyboardAvoidingView style={[base.block]} enabled={false}>
          <Navigation
            setCurrentRouteName={value => {
              dispatch(setCurrentRouteName(value));
            }}
          />
        </KeyboardAvoidingView>
      </SafeAreaView>
      <Atoms.Loader loading={stateGlobal.loading} />
    </>
  );
};

export default Containers;
