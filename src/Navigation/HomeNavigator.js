import React from 'react';
import {StyleSheet} from 'react-native';

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {useSafeAreaInsets} from 'react-native-safe-area-context';

import Profile from '../Screens/Profile/Profile';
import color from '../Constants/Color';

import Products from '../Screens/Products/Products';
import Users from '../Screens/Users/Users';

import ProductIcon from '../assets/ic_list.svg';
import ProductIconEnable from '../assets/ic_list_enable.svg';

import UsersIcon from '../assets/icn_users.svg';
import UsersIconEnable from '../assets/icn_users_enable.svg';

import ProfileIcon from '../assets/ic_profile.svg';
import ProfileIconEnable from '../assets/ic_profile_enable.svg';

const Tab = createBottomTabNavigator();

const HomeNavigator = () => {
  const {bottom} = useSafeAreaInsets();

  const tabBarListeners = ({navigation, route}) => ({
    tabPress: () => navigation.navigate(route.name),
  });

  return (
    <Tab.Navigator
      tabBarOptions={{
        tintColor: color._mediumGray,
        activeTintColor: color._blue,
        style: {
          paddingBottom: bottom + 4,
          paddingTop: 4,
          shadowColor: color._blue,
          shadowOffset: {
            width: 0,
            height: 3,
          },
          shadowOpacity: 0.27,
          shadowRadius: 4.65,
          elevation: 6,
        },
        lazy: false,
      }}>
      <Tab.Screen
        name={'Products'}
        component={Products}
        options={{
          tabBarStyle: {paddingBottom: 5},
          headerShown: false,
          tabBarLabel: 'Products',
          tabBarIcon: ({color, focused}) =>
            focused ? (
              <ProductIconEnable width={20} height={20} />
            ) : (
              <ProductIcon width={20} height={20} />
            ),
        }}
        listeners={tabBarListeners}
      />

      <Tab.Screen
        name={'Users'}
        component={Users}
        options={{
          tabBarStyle: {paddingBottom: 5},
          headerShown: false,
          tabBarLabel: 'Users',
          tabBarIcon: ({color, focused}) =>
            focused ? (
              <UsersIconEnable width={20} height={20} />
            ) : (
              <UsersIcon width={20} height={20} />
            ),
        }}
        listeners={tabBarListeners}
      />

      <Tab.Screen
        name={'Profile'}
        component={Profile}
        options={{
          tabBarStyle: {paddingBottom: 5},
          headerShown: false,
          tabBarLabel: 'Profile',
          tabBarIcon: ({color, focused}) =>
            focused ? (
              <ProfileIconEnable width={20} height={20} fill={color} />
            ) : (
              <ProfileIcon width={20} height={20} fill={color} />
            ),
        }}
        listeners={tabBarListeners}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  chatContainer: {
    padding: 15,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 18,
  },
  absoluteContainer: {position: 'absolute'},
});
export default HomeNavigator;
