import * as React from 'react';
import {NavigationContainer, CommonActions} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import StartUpScreen from '../Screens/StartUpScreen';
import LoginScreen from '../Screens/LoginScreen/LoginScreen';
import AddProduct from '../Screens/Products/AddProduct';
import AddUser from '../Screens/Users/AddUser';
import Map from '../Screens/Users/Map';

import HomeNavigator from './HomeNavigator';

const Stack = createNativeStackNavigator();

export const navigationRef = React.createRef();
export const navigate = (name, params) => {
  navigationRef.current?.dispatch(
    CommonActions.reset({
      index: 0,
      routes: [{name: 'Login'}],
    }),
  );
};

const Navigation = props => {
  const routeNameRef = React.useRef();
  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() =>
        (routeNameRef.current = navigationRef.current.getCurrentRoute().name)
      }
      onStateChange={() => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = navigationRef.current.getCurrentRoute().name;
        props.setCurrentRouteName(currentRouteName);
      }}>
      <Stack.Navigator initialRouteName={'Splash'}>
        <Stack.Screen
          options={{headerShown: false}}
          name="StartUp"
          component={StartUpScreen}
        />

        <Stack.Screen
          options={{headerShown: false}}
          name="Login"
          component={LoginScreen}
        />

        <Stack.Screen
          options={{headerShown: false}}
          name="Home"
          component={HomeNavigator}
        />

        <Stack.Screen
          options={{headerShown: false}}
          name="AddProduct"
          component={AddProduct}
        />

        <Stack.Screen
          options={{headerShown: false}}
          name="AddUser"
          component={AddUser}
        />

        <Stack.Screen
          options={{headerShown: false}}
          name="Map"
          component={Map}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Navigation;
