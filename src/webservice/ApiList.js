import HTTP from './http';

class APIList {
  constructor() {}

  login(params) {
    return HTTP.POST('auth/login', params);
  }

  getProducts(params) {
    return HTTP.GET('products');
  }
  getCategories(params) {
    return HTTP.GET('products/categories');
  }
  addProduct(params) {
    return HTTP.POST('products/add', params);
  }
  searchProduct(params, query) {
    return HTTP.GET(`products/search?q=${query}`);
  }

  getUsers(params) {
    return HTTP.GET('users');
  }
  addUser(params) {
    return HTTP.POST('users/add', params);
  }
  searchUser(params, query) {
    return HTTP.GET(`users/search?q=${query}`);
  }
}

export default new APIList();
