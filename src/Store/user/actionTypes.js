export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

export const LOGOUT_SUCCESS = 'LOGOUT_SUCCESS';
export const LOGOUT_ERROR = 'LOGOUT_ERROR';

export const GET_USER_LIST_SUCCESS = 'GET_USER_LIST_SUCCESS';
export const GET_USER_LIST_ERROR = 'GET_USER_LIST_ERROR';

export const GET_SEARCH_USER_LIST_SUCCESS = 'GET_SEARCH_USER_LIST_SUCCESS';
export const GET_SEARCH_USER_LIST_ERROR = 'GET_SEARCH_USER_LIST_ERROR';

export const ADD_USER_SUCCESS = 'ADD_USER_SUCCESS';
export const ADD_USER_ERROR = 'ADD_USER_ERROR';
export const RESET_ADD_STATUS = 'RESET_ADD_STATUS';
