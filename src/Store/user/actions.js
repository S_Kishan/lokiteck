import AsyncStorage from '@react-native-async-storage/async-storage';
import * as types from './actionTypes';
import ApiList from '../../webservice/ApiList';
import {AlertHelper} from '../../Constants/AlertHelper';
import {apiLoadingStart, apiLoadingStop} from '../global';

export const userLoginAction = (request, navigation) => {
  return dispatch => {
    dispatch(apiLoadingStart());
    return ApiList.login(request)
      .then(response => {
        if (response.token !== null) {
          dispatch({
            type: types.LOGIN_SUCCESS,
            payload: response,
          });
          AsyncStorage.setItem('Token', response.token);
          navigation.navigate('Home', {screen: 'AirDuctCleaning'});
        } else {
          dispatch({
            type: types.LOGIN_ERROR,
            payload: response.message,
          });
        }
        if (response.message) {
          AlertHelper.long(response.message);
        }
        dispatch(apiLoadingStop());
      })
      .catch(error => {
        dispatch({type: types.LOGIN_ERROR, payload: error});
        dispatch(apiLoadingStop());
      });
  };
};

export const userLogoutAction = (request, navigation) => {
  return dispatch => {
    dispatch(apiLoadingStart());
    dispatch({type: types.LOGOUT_SUCCESS});
    AsyncStorage.setItem('Token', null);
    navigation.navigate('Login');
    dispatch(apiLoadingStop());
  };
};

export const getAllUsers = (request, navigation) => {
  return dispatch => {
    dispatch(apiLoadingStart());
    return ApiList.getUsers()
      .then(response => {
        if (response) {
          dispatch({
            type: types.GET_USER_LIST_SUCCESS,
            payload: response.users,
          });
        } else {
          dispatch({
            type: types.GET_USER_LIST_ERROR,
            payload: response.message,
          });
        }
        if (response.message) {
          AlertHelper.long(response.message);
        }
        setTimeout(() => {
          dispatch(apiLoadingStop());
        }, 2000);
      })
      .catch(error => {
        dispatch({type: types.GET_USER_LIST_ERROR, payload: error});
        dispatch(apiLoadingStop());
      });
  };
};

export const getSearchUsers = (request, navigation) => {
  return dispatch => {
    dispatch(apiLoadingStart());
    return ApiList.searchUser(request, request.query)
      .then(response => {
        if (response) {
          dispatch({
            type: types.GET_SEARCH_USER_LIST_SUCCESS,
            payload: response.users,
          });
        } else {
          dispatch({
            type: types.GET_SEARCH_USER_LIST_ERROR,
            payload: response.message,
          });
        }
        if (response.message) {
          AlertHelper.long(response.message);
        }
        dispatch(apiLoadingStop());
      })
      .catch(error => {
        dispatch({type: types.GET_SEARCH_USER_LIST_ERROR, payload: error});
        dispatch(apiLoadingStop());
      });
  };
};

export const addUserAction = (request, navigation) => {
  return dispatch => {
    dispatch(apiLoadingStart());
    return ApiList.addUser(request)
      .then(response => {
        if (response !== null) {
          dispatch({
            type: types.ADD_USER_SUCCESS,
            payload: response,
          });
          navigation.goBack();
        } else {
          dispatch({
            type: types.ADD_USER_ERROR,
            payload: response.message,
          });
        }
        if (response.message) {
          AlertHelper.long(response.message);
        }
        dispatch(apiLoadingStop());
      })
      .catch(error => {
        dispatch({type: types.ADD_USER_ERROR, payload: error});
        dispatch(apiLoadingStop());
      });
  };
};

export const resetAddStatus = () => {
  return dispatch => {
    dispatch({type: types.RESET_ADD_STATUS});
  };
};
