import {confirmButtonStyles} from 'react-native-modal-datetime-picker';
import {AlertHelper} from '../../Constants/AlertHelper';
import * as types from './actionTypes';

const initialReducer = {
  currentUser: {},
  token: '',
  loading: false,
  isError: false,
  errorMsg: null,
  isLogin: false,

  userList: {},
  addedUser: {},
  searchUserList: {},
  addSuccess: false,
};

const userReducer = (state = initialReducer, action) => {
  switch (action.type) {
    case types.LOGIN_SUCCESS:
      return {
        ...state,
        currentUser: action.payload,
        token: action.payload.token,
        isLogin: true,
      };
    case types.LOGIN_ERROR:
      return {
        ...state,
        isError: true,
        errorMsg: action.payload,
      };

    case types.GET_USER_LIST_SUCCESS:
      return {
        ...state,
        userList: action.payload,
      };

    case types.GET_USER_LIST_ERROR:
      return {
        ...state,
        userList: {},
        isError: true,
      };

    case types.GET_SEARCH_USER_LIST_SUCCESS:
      return {
        ...state,
        userList: action.payload,
      };
    case types.GET_SEARCH_USER_LIST_ERROR:
      return {
        ...state,
        userList: {},
        isError: true,
      };

    case types.ADD_USER_SUCCESS:
      return {
        ...state,
        addSuccess: true,
        addedUser: action.payload,
      };

    case types.RESET_ADD_STATUS:
      return {
        ...state,
        addSuccess: false,
      };

    case types.LOGOUT_SUCCESS:
      AlertHelper.long('User Logout Success.');
      return {
        ...state,
        currentUser: {},
        token: '',
        loading: false,
        isError: false,
        errorMsg: null,
        isLogin: false,
        userList: {},
        addedUser: {},
        searchUserList: {},
        addSuccess: false,
      };
    case types.LOGOUT_ERROR:
      return {
        ...state,
        isError: true,
        errorMsg: action.payload,
      };

    default:
      return state;
  }
};

export default userReducer;
