import {combineReducers} from 'redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {persistReducer} from 'redux-persist';

import user from './user';
import global from './global';
import products from './products';

const config = {
  key: 'root',
  debug: true,
  storage: AsyncStorage,
};

const AppReducers = combineReducers({
  global,
  user,
  products
});

const rootReducer = (state, action) => {
  if (action.type === 'LOGOUT_SUCCESS') {
    return AppReducers(undefined, action);
  }
  return AppReducers(state, action);
};

const pReducer = persistReducer(config, rootReducer);

export default pReducer;
