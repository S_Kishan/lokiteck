import * as types from './actionTypes';
import ApiList from '../../webservice/ApiList';
import {AlertHelper} from '../../Constants/AlertHelper';
import {apiLoadingStart, apiLoadingStop} from '../global';

export const getAllProducts = (request, navigation) => {
  return dispatch => {
    dispatch(apiLoadingStart());
    return ApiList.getProducts()
      .then(response => {
        if (response) {
          dispatch({
            type: types.GET_PRODUCT_LIST_SUCCESS,
            payload: response.products,
          });
        } else {
          dispatch({
            type: types.GET_PRODUCT_LIST_ERROR,
            payload: response.message,
          });
        }
        if (response.message) {
          AlertHelper.long(response.message);
        }
        dispatch(apiLoadingStop());
      })
      .catch(error => {
        dispatch({type: types.GET_PRODUCT_LIST_ERROR, payload: error});
        dispatch(apiLoadingStop());
      });
  };
};

export const getProductsCategories = (request, navigation) => {
  return dispatch => {
    dispatch(apiLoadingStart());
    return ApiList.getCategories()
      .then(response => {
        if (response) {
          dispatch({
            type: types.GET_PRODUCT_CATEGORY_LIST_SUCCESS,
            payload: response,
          });
        } else {
          dispatch({
            type: types.GET_PRODUCT_CATEGORY_LIST_ERROR,
            payload: response.message,
          });
        }
        if (response.message) {
          AlertHelper.long(response.message);
        }
        dispatch(apiLoadingStop());
      })
      .catch(error => {
        dispatch({
          type: types.GET_PRODUCT_CATEGORY_LIST_ERROR,
          payload: error,
        });
        dispatch(apiLoadingStop());
      });
  };
};

export const getSearchProducts = (request, navigation) => {
  return dispatch => {
    dispatch(apiLoadingStart());
    return ApiList.searchProduct(request, request.query)
      .then(response => {
        if (response) {
          dispatch({
            type: types.GET_SEARCH_PRODUCT_LIST_SUCCESS,
            payload: response.products,
          });
        } else {
          dispatch({
            type: types.GET_SEARCH_PRODUCT_LIST_ERROR,
            payload: response.message,
          });
        }
        if (response.message) {
          AlertHelper.long(response.message);
        }
        dispatch(apiLoadingStop());
      })
      .catch(error => {
        dispatch({type: types.GET_SEARCH_PRODUCT_LIST_ERROR, payload: error});
        dispatch(apiLoadingStop());
      });
  };
};

export const addProductAction = (request, navigation) => {
  return dispatch => {
    dispatch(apiLoadingStart());
    return ApiList.addProduct(request)
      .then(response => {
        if (response !== null) {
          dispatch({
            type: types.ADD_PRODUCT_SUCCESS,
            payload: response,
          });
          navigation.goBack();
        } else {
          dispatch({
            type: types.ADD_PRODUCT_ERROR,
            payload: response.message,
          });
        }
        if (response.message) {
          AlertHelper.long(response.message);
        }
        dispatch(apiLoadingStop());
      })
      .catch(error => {
        dispatch({type: types.ADD_PRODUCT_ERROR, payload: error});
        dispatch(apiLoadingStop());
      });
  };
};

export const resetAddStatus = () => {
  return dispatch => {
    dispatch({type: types.RESET_ADD_STATUS});
  };
};
