import * as types from './actionTypes';

const initialReducer = {
  productList: {},
  addedProduct: {},
  searchProductList: {},
  addSuccess: false,
  categories: [],
  loading: false,
  isError: false,
  errorMsg: null,
};

const productReducer = (state = initialReducer, action) => {
  switch (action.type) {
    case types.GET_PRODUCT_LIST_SUCCESS:
      return {
        ...state,
        productList: action.payload,
      };

    case types.GET_PRODUCT_LIST_ERROR:
      return {
        ...state,
        productList: {},
        isError: true,
      };

    case types.GET_SEARCH_PRODUCT_LIST_SUCCESS:
      return {
        ...state,
        productList: action.payload,
      };
    case types.GET_SEARCH_PRODUCT_LIST_ERROR:
      return {
        ...state,
        productList: {},
        isError: true,
      };

    case types.GET_PRODUCT_CATEGORY_LIST_SUCCESS:
      return {
        ...state,
        categories: action.payload,
      };

    case types.GET_PRODUCT_CATEGORY_LIST_ERROR:
      return {
        ...state,
        categories: [],
      };

    case types.ADD_PRODUCT_SUCCESS:
      return {
        ...state,
        addSuccess: true,
        addedProduct: action.payload,
      };

    case types.RESET_ADD_STATUS:
      return {
        ...state,
        addSuccess: false,
      };

    case types.LOGOUT_SUCCESS:
      return {
        ...state,
        productList: {},
        addedProduct: {},
        searchProductList: {},
        addSuccess: false,
        categories: [],
        loading: false,
        isError: false,
        errorMsg: null,
      };

    default:
      return state;
  }
};

export default productReducer;
