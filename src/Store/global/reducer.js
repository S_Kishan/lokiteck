import * as types from './actionTypes';

const initialReducer = {
  currentRouteName: '',
  furnanceLocationList: [],
  insulationStatusList: [],
  insulationTypesList: [],
  docFiles: [],
  loading: false,
  isError: false,
  isInternetConnected: true,
  isForceFeedback: false,
  isFeedbackSubmited: null,
};

const globalReducer = (state = initialReducer, action) => {
  switch (action.type) {
    case types.API_LOADING_START:
      return {
        ...state,
        loading: true,
      };
    case types.API_LOADING_STOP:
      return {
        ...state,
        loading: false,
      };

    case types.IS_INTERNET_CONNECTED:
      if (action.payload === false) {
        return {
          ...state,
          isInternetConnected: action.payload,
        };
      } else {
        return {
          ...state,
          isInternetConnected: action.payload,
        };
      }
    case types.SET_CURRENT_ROUTE_NAME:
      return {
        ...state,
        currentRouteName: action.payload,
      };

    case types.LOGOUT_SUCCESS:
      return {
        ...state,
        currentRouteName: '',
        furnanceLocationList: [],
        insulationStatusList: [],
        insulationTypesList: [],
        docFiles: [],
        loading: false,
        isError: false,
        isForceFeedback: false,
      };

    default:
      return state;
  }
};

export default globalReducer;
