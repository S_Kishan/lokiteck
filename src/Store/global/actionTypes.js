export const API_LOADING_START = 'API_LOADING_START';
export const API_LOADING_STOP = 'API_LOADING_STOP';
export const IS_INTERNET_CONNECTED = 'IS_INTERNET_CONNECTED';

export const SET_CURRENT_ROUTE_NAME = 'SET_CURRENT_ROUTE_NAME';
