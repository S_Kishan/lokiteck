import {StyleSheet} from 'react-native';
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from 'react-native-responsive-screen';
import color from './Color';

const base = StyleSheet.create({
  block: {
    flex: 1,
  },
  block_blue: {
    flex: 1,
    backgroundColor: color._blue,
  },
  horizontal: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  center: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  smallSpaceInside: {
    padding: wp(1.25),
  },
  mediumSpaceInside: {
    padding: wp(2.75),
  },
  largeSpaceInside: {
    padding: wp(4.25),
  },
  smallSpaceOutSide: {
    margin: wp(1.25),
  },
  mediumSpaceOutSide: {
    margin: wp(1.25),
  },
  largeSpaceOutSide: {
    margin: wp(1.25),
  },
  horizonalSpace: {
    paddingHorizontal: wp(5),
  },
  fontSmall: {
    fontSize: wp(3.25),
    fontWeight: 'bold',
  },
  fontMedium: {
    color: color._black,
    fontSize: wp(4.25),
  },
  fontBold: {
    color: color._black,
    fontSize: wp(4.25),
    fontWeight: 'bold',
  },
  fontLarge: {
    fontSize: wp(6.5),
    color: color._black,
    fontWeight: 'bold',
  },
  hilitedFont: {
    fontSize: wp(3.25),
    color: color._blue,
    fontWeight: '700',
  },
  descriptionTxt: {
    color: color._black,
    fontSize: wp(3.5),
  },
  buttonTxt: {
    color: color._white,
    fontSize: wp(4.25),
    fontWeight: 'bold',
  },
  border: {
    borderRadius: wp(2.75),
    borderColor: color._lightGray,
  },
  deviderLine: {
    height: 1,
    width: '100%',
    backgroundColor: color._mediumGray,
    marginTop: wp(1.5),
  },
});

export default base;
