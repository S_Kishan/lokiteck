const fonts = {
  BLACK: 'SFProDisplay-Black',
  BLACK_ITALIC: 'SFProDisplay-BlackItalic',
  BOLD: 'SFProDisplay-Bold',
  BOLD_ITALIC: 'SFProDisplay-BoldItalic',
  HEAVY: 'SFProDisplay-Heavy',
  HEAVY_ITALIC: 'SFProDisplay-HeavyItalic',
  LIGHT: 'SFProDisplay-Light',
  LIGHT_ITALIC: 'SFProDisplay-LightItalic',
  MEDIUM: 'SFProDisplay-Medium',
  MEDIUM_ITALIC: 'SFProDisplay-MediumItalic',
  REGULAR: 'SFProDisplay-Regular',
  REGULAR_ITALIC: 'SFProDisplay-RegularItalic',
  SEMI_BOLD: 'SFProDisplay-Semibold',
  SEMI_BOLD_ITALIC: 'SFProDisplay-SemiboldItalic',
  THIN: 'SFProDisplay-Thin',
  THIN_ITALIC: 'SFProDisplay-ThinItalic',
  ULTRA_LIGHT: 'SFProDisplay-Ultralight',
  ULTRA_LIGHT_ITALIC: 'SFProDisplay-UltralightItalic',
};

export default fonts;
