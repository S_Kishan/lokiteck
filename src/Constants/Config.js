export const HeaderSecretKey = {
    Development: '',
    Testing: '',
    Production: '',
    Alpha: '',
  };
  export const AppVersion = 'v0.0.1';
  
  export const mode = 'development';
  
  export const apiConfig = {
    productionApiURL: '',
    testingApiURL: '',
    developmentApiURL:
      'https://dummyjson.com/',
  };
  
  