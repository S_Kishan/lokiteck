const color = {
  _white: '#ffffff',
  _black: '#000000',
  _red: '#ff0000',
  _gray: '#555555',
  _blue: '#0d59a2',
  _lightGray: '#e5e5e5',
  _mediumGray: '#c5c5c5',
  _green: '#9fce67',
  _transparent: 'transparent',
};

export default color;
